package Base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.ResourceBundle;

public class  CreateDriverInstance {

     ResourceBundle resource;
     WebDriver driver=null;
    public  WebDriver generateDriverInstance(){


                resource=ResourceBundle.getBundle("config");
                if(resource.getString("browser").equals("chrome")) {

                    WebDriverManager.chromedriver().setup();
                    driver=new ChromeDriver();
                    driver.manage().window().maximize();

                }
                else if(resource.getString("browser").equals("firefox")) {

                    WebDriverManager.firefoxdriver().setup();
                    driver=new FirefoxDriver();
                    driver.manage().window().maximize();

                }
                else if(resource.getString("browser").equals("ie")) {

                    WebDriverManager.iedriver().setup();
                    driver=new InternetExplorerDriver();
                    driver.manage().window().maximize();

                }
                else
                {
                    WebDriverManager.chromedriver().setup();
                    driver=new ChromeDriver();
                    driver.manage().window().maximize();
                }

                driver.get(resource.getString("applicationURL"));

                return driver;
            }

            public void tear_down(WebDriver driver) {

                driver.close();
            }
        }





