package WebPages;

import Utility.Xpaths;
import cucumber.api.java.eo.Se;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HomePage {
    WebDriver driver = null;


    public HomePage(WebDriver driver) {
        //Test_Drive
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = Xpaths.search_input)
    public WebElement search_input;
    @FindBy(xpath = Xpaths.min_price)

    public WebElement min_price;
    @FindBy(xpath = Xpaths.search_btn)
    public WebElement search_btn;
  /*  @FindBy(how = How.XPATH, using = "//span[contains(text(),'For sale')]")
    public WebElement forsale;

    @FindBy(xpath = Xpaths.for_sale)
    public WebElement for_sale;

    @FindBy(xpath = Xpaths.added)
    public WebElement added;


    @FindBy(xpath = Xpaths.advanced_toggle)
    public WebElement advanced_toggele;

    @FindBy(xpath = Xpaths.auctions)
    public WebElement auctions;

    @FindBy(xpath = Xpaths.bedroom)
    public WebElement bedroom;


    @FindBy(xpath = Xpaths.sort_by)
    public WebElement sort_by;

    @FindBy(xpath = Xpaths.distance_from_location)
    public WebElement distance_from_location;

    @FindBy(xpath = Xpaths.max_price)
    public WebElement max_price;

    @FindBy(xpath = Xpaths.min_price)
    public WebElement min_price;

    @FindBy(xpath = Xpaths.keywords)
    public WebElement keyword;


    @FindBy(xpath = Xpaths.property_type)
    public WebElement property_type;


    @FindBy(xpath = Xpaths.new_homes)
    public WebElement new_homes;




    @FindBy(xpath = Xpaths.under_offer)
    public WebElement under_offer;

    @FindBy (xpath = Xpaths.shared_ownership)
    public WebElement shared_ownership;



    @FindBy (xpath = Xpaths.retirement_homes)
    public WebElement retirement_homes;

*/


    public void searchInput(String searchValue) {
        search_input.sendKeys(searchValue);

    }

    public void clickMinPrice(int a) {
        Select min = new Select(min_price);
        min.selectByIndex(a);
    }

    public void clickPropertyType() {
        //property_type.click();
    }

    public void clickSearchBtn() {

        search_btn.click();
    }

    public String navigateNextPage() {
        System.out.println("uuu");

        String url = driver.getCurrentUrl();
        return url;
    }

}
