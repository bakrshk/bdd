package Utility;

public class Xpaths {


/*
                                     Element Location of Home Page

 */
    public static final String  for_sale="//span[contains(text(),'For sale')]";
    public static final String  search_input="//input[@id='search-input-location']";

    public static final String  search_btn = "//button[@id='search-submit']";
    public static final String min_price="//select[@id='forsale_price_min']";
    public static final String max_price="//select[@id='forsale_price_max']";
    public static final String property_type="//select[@id='property_type']";
    public static final String bedroom="//select[@id='beds_min']";
    public static final String distance_from_location ="//select[@id='radius']";
    public static final String added="//select[@id='added']";
    public static final String  sort_by="//select[@id='added']";
    public static final String keywords="//input[@id='keywords']";
    public static final String new_homes="//div[@class='form-group search-attr-for-sale ']//label[contains(text(),'New homes')]";
    public static final String retirement_homes="//div[@class='form-group search-attr-for-sale ']//label[contains(text(),'Retirement homes')]";
    public static final String shared_ownership="//div[@class='form-group search-attr-for-sale ']//label[contains(text(),'Shared ownership')]";
    public static final String auctions="//div[@class='form-group search-attr-for-sale ']//label[contains(text(),'Auctions')]";
    public static final String under_offer="//div[@class='form-group search-attr-for-sale ']//label[contains(text(),'Under offer or sold STC')]";
    public static final String advanced_toggle="//a[@class='search-advanced-toggle link']";

/*

                                    Elements of LoginPage


 */
}
