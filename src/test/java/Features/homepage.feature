
Feature: Zoopla Search functionality

Background:
  Given User is already on searching page
  When User enter a searching value in search field

  Scenario: Search For sale without Minimum Price

    When User doesnt select minimum price
    And User click on the search button

    Then User should see all properties list



    Scenario: Search For sale Minimum Price

    When User select minimum price
    And User click on the search button
    Then User should see all properties list
