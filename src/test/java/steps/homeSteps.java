package steps;

import Base.CreateDriverInstance;
import WebPages.BasePage;
import WebPages.HomePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class homeSteps {
    public static Logger log = LogManager.getLogger(homeSteps.class.getName());

    CreateDriverInstance browser = new CreateDriverInstance();

    WebDriver Test_driver=null;

    HomePage homePage;

    @Given("User is already on searching page")
    public void userIsAlreadyOnSearchingPage() {

       Test_driver=browser.generateDriverInstance();
        homePage = new HomePage(Test_driver) ;
        log.info("Driver is Initilized");
    }

    @When("User enter a searching value in search field")
    public void userEnterASearchingValueInSearchField() {
        homePage.searchInput("London");
    }

    @When("User select minimum price")
    public void userSelectMinimumPrice() {
        homePage.clickMinPrice(1);
    }

    @And("User click on the search button")
    public void userClickOnTheSearchButton() {
        homePage.clickSearchBtn();

    }

    @Then("User should see all properties list")
    public void userShouldSeeAllPropertiesList() {
        String abc=  homePage.navigateNextPage();
        System.out.println(abc);


    }

    @When("User doesnt select minimum price")
    public void userDoesntSelectMinimumPrice() {

    }
}